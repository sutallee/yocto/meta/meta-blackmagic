# /home/mike/yocto/gatesgarth/pizw/tmp/work/arm1176jzfshf-vfp-poky-linux-gnueabi/blackmagic/0.1-r0/git
SUMMARY = "Recipe to build a network server for the Black Magic Probe"
DESCRIPTION = "Recipe created by Mike Moran"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit pkgconfig

FILES_${PN} += " \
	${systemd_system_unitdir}/* \
	${systemd_system_unitdir}/multi-user.target.wants/* \
	${base_libdir_native}/udev/rules.d/* \
	"

# The DEPENDS lists packages that are required
# at *build* time including things like: header
# files; source code; and libraries, etc.
DEPENDS = "libusb1 libftdi hidapi"

# The RDEPENDS lists packages that are required
# at run-time.
RDEPENDS_${PN} = "libusb1 libftdi hidapi"

# The blackmagic recipe currently requires systemd.
# There is no support for sysvinit.
REQUIRED_DISTRO_FEATURES = "systemd"


SRCREV = "cf5b4afb3840bc2d20c29bf0843c65ebb0fb1a54"

SRC_URI = " \
	git://github.com/blacksphere/blackmagic.git \
	file://blackmagic.patch \
	"

python do_build() {
    bb.plain("***********************************************");
    bb.plain("*                                             *");
    bb.plain("*  Example recipe created by bitbake-layers   *");
    bb.plain("*                                             *");
    bb.plain("***********************************************");
}



do_patch() {
	pushd git
	patch -p1 < ../blackmagic.patch
	popd
}

do_configure() {
	# No configuration to run
	return 0
}

do_compile() {
	pushd ../git
	make PROBE_HOST=hosted
	popd
}

do_clean() {
	pushd ${S}/../git
	echo "PWD: $PWD"
	make PROBE_HOST=hosted clean
	popd
}

do_install() {
	echo "1"
	install -d ${D}${bindir}
	echo "2"
	install -m 0755 ${S}/../git/src/blackmagic ${D}${bindir}/blackmagic

	echo "3"
	# This test is really not necessary if REQUIRED_DISTRO_FEATURES
	# works as advertised.
	if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'true', 'false', d)}; then
		echo "4"
		# create the /lib/systemd/system directory
		install -d ${D}${systemd_system_unitdir}

		# install the service
		# target: /lib/systemd/system/blackmagic-service@.service
		# source: git/lib/systemd/system/blackmagic-service@.service

		echo "5"
		echo "S : ${S}"
		echo "D : ${D}"
		install -m 0644 ${S}/../git/lib/systemd/system/blackmagic-service@.service ${D}${systemd_system_unitdir}/blackmagic-service@.service

		# systemd symbolic link
		# /lib/systemd/system/multi-user.target.wants/blackmagic-service@probe.service
		# to ../blackmagic-service@.service
		# target: /lib/systemd/system/multi-user.target.wants/blackmagic-service@probe.service
		# source: none
		#
		# ln -s ../blackmagic-service@.service /lib/systemd/system/multi-user.target.wants/blackmagic-service@probe.service

		echo "6"
		install -d ${D}${systemd_system_unitdir}/multi-user.target.wants

		echo "7"
		pushd ${D}/${systemd_system_unitdir}/multi-user.target.wants
		echo "8"
		ln -s ../blackmagic-service@.service blackmagic-service@probe.service
		echo "9"
		popd
		echo "10"
	else
		echo "This recipe requires systemd."
	fi

	# udev rule
	# target: /lib/udev/rules.d/99-usb-serial-blackmagic.rules
	# source: git/lib/udev/rules.d/99-usb-serial-blackmagic.rules
	echo "11"
	install -d ${D}${base_libdir_native}/udev/rules.d
	echo "12"
	install -m 0644 ${S}/../git/lib/udev/rules.d/* ${D}${base_libdir_native}/udev/rules.d
	echo "13"
}

